import { Given } from 'cypress-cucumber-preprocessor/steps';

Given('I visit url {string}', (urlPath: string) => {
    cy.visit(urlPath);
});

Given('I see {string} title on the page', (title: string) => {
    cy.get('h1').should('have.text',title);
});