Feature: Website loading

    Our website should appear when url is visited

    Scenario: Visiting todo app
        Given I visit url "/"
        Then I see "todos" title on the page
